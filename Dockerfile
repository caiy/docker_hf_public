FROM gitlab-registry.cern.ch/caiy/histfitter:latest
ENTRYPOINT /bin/sh

USER root
RUN useradd -rm -d /home/atlas -s /bin/bash -g root -G sudo -u 500 atlas
RUN usermod -G root atlas

USER atlas

WORKDIR /home
